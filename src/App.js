import React from 'react';
import './App.css';
import './icofont.css';

import Background from './components/Background/background';
import Header from './components/Header/header'
import Locker from './components/Locker/locker'


function App() {
  return (
      <div className="background_img">
          <Header/>          
          <Locker/> 
          
      </div>
          
  );
}

export default App;
