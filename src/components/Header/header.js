import React from 'react';
import logo from '../../img/UD_Logo_White.svg';
import './header.css';

function Header() {
    return (
        <div className="header_box">
            <div>
            <img src={logo} className="logo_top"></img>
            </div>

            <div className="white_font">
                <i className="icofont-signal icons"></i>
                <i className="icofont-wifi icons"></i>
                <i className="icofont-battery-full icons"></i>
            </div>
        </div>
    )
}

export default Header;
