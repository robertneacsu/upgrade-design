import React from 'react';
import './locker.css';

import InlineSVG from "inline-svg-react";
import iconSVG from "../../img/lock.svg";
import { ReactComponent as Lock } from "../../img/lock.svg";


function Locker() {
    const Icon = '<svg><path d="M13 23h7V8L10 .631 0 8v15h7v-7h6v7z"/></svg>'

    return (
        <div className="aling_center">

            {/* <img src={iconSVG} className="locker_icon"/> */}
            <Lock />
            <InlineSVG icon={Icon} label="Home" />

        </div>
    )
}

export default Locker;
